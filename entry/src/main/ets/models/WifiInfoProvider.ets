import wifiManager from '@ohos.wifiManager';
import data from '@ohos.telephony.data';
import network from '@system.network';

export enum WifiConnectionState {
  WIFI_STATE_DISCONNECTED,
  WIFI_STATE_DISCONNECTING,
  WIFI_STATE_CONNECTING,
  WIFI_STATE_CONNECTED,
}


@Observed
class WifiIpv4Info {
  public ip: string = ""
  public gateway: string = ""
  public dns_1: string = ""
  public dns_2: string = ""
}


@Observed
export class WifiInfoProvider {
  public wifi_switch_on: boolean = false;
  public wifi_state: WifiConnectionState = WifiConnectionState.WIFI_STATE_DISCONNECTED
  public wifi_state_str: string

  public show_v4_info:boolean = true;

  public v4_info: WifiIpv4Info = new WifiIpv4Info()
  public ip: string = ""
  public gateway: string = ""
  public netmask: string = ""
  public dns_1: string = ""
  public dns_2: string = ""
  public country_code:string = ""



  constructor() {
    this.wifi_state_str = "unknown"
    // this.initialize() //dont do this
  }
  initialize(){
    wifiManager.on("wifiStateChange", state => {
      this.wifiStateChangedEvent(state)
    })

    // on scanning
    wifiManager.on("wifiRssiChange", async rssi=>{
      console.log("wifi rssi change to: " + rssi)
    })

    wifiManager.on("wifiConnectionChange", state=>{
      //0: disconnected, 1: connected
      console.log("wifi ConnectionChange to: " + state)
      this.wifiConnectionChangedEvent(state)
    })

    if(wifiManager.isConnected()){
      this.wifi_state = WifiConnectionState.WIFI_STATE_CONNECTED
      this.wifi_switch_on = true
      this.updateWifiInfo()
    }else{
      this.wifi_state = WifiConnectionState.WIFI_STATE_DISCONNECTED
      this.updateWifiLinkStatus()
    }
  }

  private updateWifiLinkStatus(){
    let manager = getContext(this).resourceManager;
    let key: Resource;
    switch (this.wifi_state){
      case WifiConnectionState.WIFI_STATE_DISCONNECTED:
        key = $r('app.string.wifi_state_disconnected')
        break
      case WifiConnectionState.WIFI_STATE_DISCONNECTING:
        key = $r('app.string.wifi_state_disconnecing')
        break;
      case WifiConnectionState.WIFI_STATE_CONNECTING:
        key = $r('app.string.wifi_state_connecting')
        break;
      case WifiConnectionState.WIFI_STATE_CONNECTED:
        key = $r('app.string.wifi_state_connected')
        break;
      default:
        console.log("unkown wifi state, can not generate new state string");
        return
    }
    manager.getStringValue(key.id, (error, value)=> {
      if(error != null){
        console.error("getStringValue failed: "+error)
      }else{
        console.log("wifi state change to value: "+value);
        this.wifi_state_str = value;
      }
    })
  }

  private updateWifiInfo(){
    this.updateWifiLinkStatus()
    this.ip = this.getAddressString(wifiManager.getIpInfo().ipAddress)
    this.gateway = this.getAddressString(wifiManager.getIpInfo().gateway)
    this.netmask = this.getAddressString(wifiManager.getIpInfo().netmask)
    this.dns_1 = this.getAddressString(wifiManager.getIpInfo().primaryDns)
    this.dns_2 = this.getAddressString(wifiManager.getIpInfo().secondDns)

    this.country_code = wifiManager.getCountryCode()
  }

  private wifiStateChangedEvent(state: number){
    //这个看起来表示的是WiFi开关的状态，并不表示实际连接状态
    //0: inactive, 1: active, 2: activating, 3: de-activating
    if(state == 1 || state == 2){
      this.wifi_switch_on = true
      this.wifi_state = WifiConnectionState.WIFI_STATE_CONNECTING
      console.log("wifi status change to connecting")
    }else{
      this.wifi_switch_on = false
      // Seem system send disconnecting after disconnected event, ignore it.
      // this.wifi_state = WifiConnectionState.WIFI_STATE_DISCONNECTING
      console.log("wifi status change to disconnecting")
    }
    this.updateWifiLinkStatus()
  }

  private wifiConnectionChangedEvent(state: number){
    //0: disconnected, 1: connected
    if(state == 1){
      this.wifi_state = WifiConnectionState.WIFI_STATE_CONNECTED
      console.log("wifi status change to connected")
    }else{
      this.wifi_state = WifiConnectionState.WIFI_STATE_DISCONNECTED
      console.log("wifi status change to disconnected")
    }

    this.updateWifiInfo()
    console.log("wifi connection changed to: " + state + "(" +this.getWifiStateStr(state) +")")
  }

  private getWifiStateStr(state: number){
    let state_str: string = "unkown"
    switch(state){
      case 0:
        state_str = "inactive";
        break
      case 1:
        state_str = "active";
        break
      case 2:
        state_str = "activating";
        break
      case 3:
        state_str = "de-activating";
        break
        DEFAULT:
          console.log("unkown wifi state");
    }
    return state_str
  }

  isWifiConnected(): boolean {
    return wifiManager.isConnected();
  }

  getAddressString(address: Number): string{
    let oct1 = (address.valueOf() >> 24) & 0xFF
    let oct2 = (address.valueOf() >> 16) & 0xFF
    let oct3 = (address.valueOf() >> 8) & 0xFF
    let oct4 = (address.valueOf()) & 0xFF
    return oct1.toString() + "." + oct2.toString() + "." + oct3.toString() + "." + oct4.toString()
  }

  public switchVersion(){
    console.log("switch display ip version info")
  }
}