# OhXunlong 寻龙尺
Network Info Query application for OpenHarmony API 11+
网络信息查看工具

## 项目介绍
用于查看当前机器的Wi-Fi

### 为什么要开发这个应用？
新手入门OpenHarmony，前期不想投入太多金钱购买开发板，想先简单熟悉一下，然后我发现一加6T的CPU是骁龙845，GB跑分跟rk3588非常接近，而二手一加6T非常便宜，300出头就能收到，还要什么自行车。

但是我翻车了，我这台的电池可能不是原装的，所以刷完机有两个表现，首先是电池电量显示2800%，另一方面就是开机状态下，拔插USB都会引起系统关机。咨询了Algoldeas大佬，感觉如果要修复这个问题，要改动内核，工作量可能比较大，所以想通过无线调试的方案来当做workaround。

OpenHarmony使用命令配置为无线调试后，重启也不会丢状态，这点挺好
```bash
hdc tmode tcp 5555
```

但是设置里似乎看不到WiFi的IP地址？所以为了方便无线调试连接，我开发了这款显示网络（Wi-Fi）信息的应用

![运行截图](./md_image/ScreenShot1.jpeg)

## 开发环境
- 一加6T (OnePlus-6T-4.1-release-algoldeas)
- DevEco 4.1 Release  
- API 11

## 感谢
本项目UI部分主要参考**westinyang**大佬的[DeviceInfo](https://gitee.com/westinyang/device-info#https://gitee.com/link?target=https%3A%2F%2Fwww.iconarchive.com%2Fshow%2Fpapirus-apps-icons-by-papirus-team%2Fvirtualbox-icon.html)

感谢**Algoldeas**大佬放出的一加6T固件